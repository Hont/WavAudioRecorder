# WavAudioRecorder

Unity3D下的wav文件生成和输出demo，可以录制Unity3D中的声音并输出wav文件。 
参考自官方的FrameCapturer，并将C++部分拆回了C#中，方便使用。 **(后回顾补充：正确应用BinaryWriter即可避免二进制写入的种种问题，因该仓库创建较早，故仅做文字提示一下使用者，但代码功能无问题)** 

测试版本2017.4